import './App.css';
import SubjectInput from './components/Subjects/SubjectInput/SubjectInput';
import SubjectList from './components/Subjects/SubjectList/SubjectList';
import { useState } from "react";

function App() {

  const [enteredSubject, setEnteredSubject] = useState([
    { name: 'Maths', description: "On sait depuis longtemps que travaller avec du text lisible et ...", id: Math.random().toString() },
    { name: 'Physique', description: "On sait depuis longtemps que travaller avec du text lisible et ...", id: Math.random().toString() },
    { name: 'Arabe', description: "On sait depuis longtemps que travaller avec du text lisible et ...", id: Math.random().toString() },
    { name: 'SVT', description: "On sait depuis longtemps que travaller avec du text lisible et ...", id: Math.random().toString() },
    { name: 'Anglais', description: "On sait depuis longtemps que travaller avec du text lisible et ...", id: Math.random().toString() },

  ]);

  // const [isPopUp, setIsPopUp] = useState(false);

  const subjectHandler = (enteredName, enteredDesc) => {
    setEnteredSubject((prevSubject) => {
      return [...prevSubject, { name: enteredName, description: enteredDesc, id: Math.random().toString() }]
    });
  }
  // console.log(enteredSubject);


  const deleteItemhandler = SubjectID => {
    setEnteredSubject(prevSubject => {
      const updatedData = [...prevSubject].filter(subject => subject.id !== SubjectID);
      return updatedData;
    });
  }



  return (
    
      <div className="section">
        <h1>Taki<span style={{ color: "#04B6F6" }}>Academy </span>  <br />
          <h5>Il y a {enteredSubject.length} matiéres</h5></h1>
        <div className="input"><SubjectInput addSubject={subjectHandler} /></div>
        <div className="item"><SubjectList items={enteredSubject} onDeleteItem={deleteItemhandler} />
        </div>
      </div>


    
  );
}

export default App;
