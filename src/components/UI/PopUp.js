
import React from 'react';
import Card from './Card/Card';
import './PopUp.module.css';
import classes from './PopUp.module.css';

const PopUp = (props) => {

  return (
    <div>
      <div className={classes.backdrop} onClick={props.out} />
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2 style={{ color: "#0A0F8D" }}> {props.data.name}</h2>
        </header>
        <div className={classes.content}>
          <h5 style={{color:"#0A0F8D"}}>{props.data.description}</h5>
        </div>
        <footer className={classes.actions}>
          <button className="btn btn-danger" style={{borderRadius:"14px"}} onClick={props.delete}><h5>Supprimer</h5></button>
          <button className="btn " style={{backgroundColor:"#0A0F8D" ,borderRadius:"14px"}} onClick={props.out}><h5>Fermer</h5></button>

        </footer>
      </Card>


    </div>

  );
};

export default PopUp;