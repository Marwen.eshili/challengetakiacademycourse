import React from 'react'
import SubjectItem from '../SubjectItems/SubjectItem';
import './SubjectList.module.css'

const SubjectList = (props) => {
  return (
    <div style={{ width: "1300px" }}>

      {props.items.map((sub) => (
        <SubjectItem
          key={Math.random()}
          id={sub.id}
          onDelete={props.onDeleteItem}
          list={sub}
        >
          <h1 >{sub.name}</h1>
          <h6 >{sub.description} </h6>
        </SubjectItem>
      ))}

    </div>
  )
}

export default SubjectList
