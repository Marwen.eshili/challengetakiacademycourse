import React from 'react'
import Card from '../../UI/Card/Card'
import classes from './SubjectItem.module.css';
import './SubjectItem.module.css';
import PopUp from '../../UI/PopUp'
import { useState } from 'react';



const SubjectItem = (props) => {
  const [isPopUp, setIsPopUp] = useState(false);

  const deleteHandler = () => {
    props.onDelete(props.id);
  };

  const popUpHandler = () => {
    setIsPopUp(true);
    console.log(isPopUp)
  }

  const outHandler = () => {
    setIsPopUp(false)
    //console.log(isPopUp)

  }

  console.log(props)
  return (
    <>
      <div className="card1" onClick={() => popUpHandler()} >
        <Card className={classes.users}>

          <ul className="card-body " >
            {props.children}
          </ul>

        </Card>

      </div>
      {isPopUp && (<PopUp out={() => outHandler()} delete={deleteHandler} data={props.list} />)}
    </>
  )
}

export default SubjectItem
