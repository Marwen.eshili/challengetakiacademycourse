import React from 'react';
import  { useState } from "react";
import Card from '../../UI/Card/Card';
import classes from './SubjectInput.module.css';
import Error from '../../UI/error/Error'


const SubjectInput = (props) => {

    
        // const [isValid, setIsvalid] = useState(true);
        const [enteredName , setEnteredName] = useState('');
        const [enteredDesc , setEnteredDesc] = useState('');
        const [error, setError] = useState('');

    
    


       const subjectNameChangeHandler =(event) =>{
              setEnteredName(event.target.value);
       };
       const subjectDescriptionChangeHandler = (event) => {
        setEnteredDesc(event.target.value);

       }
       const formSubmitHandler =(event) =>{ 
        event.preventDefault();


        if (enteredName.trim().length == '' && enteredDesc.trim().length == ''  ) {
            setError({
                title: 'Invalid Name and Description ',
                message: 'Please enter a valid name and desc (non-empty values).',
              });
        } else if (enteredName.trim().length == ''  ) {
            setError({
                title: 'Invalid Name',
                message: 'Please enter a valid name (non-empty values).',
              });
        } else if (enteredDesc.trim().length =='' || enteredDesc <0  ){
            setError({
                title: 'Invalid Description',
                message: 'Please enter  Description (non-empty values).',
              });
        }
        else if (enteredName.trim().length > 0 && enteredDesc.trim().length > 0) {
            setError('')
            props.addSubject(enteredName, enteredDesc);
        }



    //     event.preventDefault();
    
    //    props.addSubject(enteredName , enteredDesc);
    //      console.log(enteredName);
    //        setEnteredName("");
    //        setEnteredDesc("");
       };

       const errorHandler = () => {
        setError(null);
      };
    //style of button
      let buttonStyle = {
                  backgroundColor :"#0A0F8D",
                  textAlign:"center",
                  fontWeight:"500",
                  fontSize:"20px",
                  width:"95%",
                }
    

    return (
        <div>
                {error && (
        <Error
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
        <Card   className={classes.input}>
        <form onSubmit={formSubmitHandler}>
            <label><h2>Ajouter Une Matière</h2></label> 
            <input type="text" placeholder="Nom" onChange={subjectNameChangeHandler} value={enteredName}></input>
            <textarea type="text" placeholder="Description" onChange={subjectDescriptionChangeHandler} value={enteredDesc}></textarea>
            <button className="btn btn-success m-2 " style={buttonStyle} type="submit">Ajouter une matière</button>
        </form>
    </Card>
    </div>
    )
}

export default SubjectInput 
